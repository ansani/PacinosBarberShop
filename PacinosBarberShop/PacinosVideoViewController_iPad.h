//
//  PacinosVideoViewController_iPad.h
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTableView.h"

@class MyTableView;

@class TwitterViewController;
@class FacebookStatusViewController;
@class YouTubeController;
@class WebViewPlayer_iPad;


@interface PacinosVideoViewController_iPad : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIAlertViewDelegate>
{
    IBOutlet MyTableView *pacinosTableView;

    TwitterViewController *twitterController;
    FacebookStatusViewController *facebookStatusViewController;
    IBOutlet YouTubeController *youTubeController;
    
    IBOutlet WebViewPlayer_iPad *webViewPlayer;
    IBOutlet UIView *biographyView;
    IBOutlet UIImageView *imageViewLeft;
    IBOutlet UIImageView *imageViewRight;    
    IBOutlet UITextView *biographyTextView;
    
    IBOutlet UISearchBar *searchBar;
    
    NSArray *bioImageArray;
    
    int currentBioImage;
    NSIndexPath *currentIndexPath;
}
@property (readwrite, retain) NSIndexPath *currentIndexPath;

@property (readwrite, retain) NSArray *bioImageArray;
@property (nonatomic, retain) FacebookStatusViewController *facebookStatusViewController;
@property (nonatomic, retain) TwitterViewController *twitterController;
@property (readwrite, retain) WebViewPlayer_iPad *webViewPlayer;


- (IBAction)biographyAction:(id)sender;
- (IBAction)videoAction:(id)sender;
- (IBAction)facebookAction:(id)sender;
- (IBAction)twitterAction:(id)sender;
- (IBAction)mailAction:(id)sender;

- (void) fadeImageLeft;
- (void) fadeImageRight;
- (UIImage *) nextBioImage;

- (NSString *)timeLabelWithDuration:(int)duration;
- (void) showYouTubeAtIndexPath:(NSIndexPath *)indexPath;
- (void) refreshWebView;

- (void) processTouch:(int)quadrant;
- (void) postOnFB:(NSString*)message;
- (void) postOnTW:(NSString*)message;

@end
