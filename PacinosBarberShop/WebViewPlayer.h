//
//  WebViewPlayer.h
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@class PacinosVideoViewController_iPhone;

@interface WebViewPlayer : UIViewController <UIWebViewDelegate>
{
    IBOutlet UIWebView *webView;
    PacinosVideoViewController_iPhone *delegate;
    
    BOOL cancelNetworkLoading;
}
@property (readwrite, retain) PacinosVideoViewController_iPhone *delegate;
@property (readwrite, retain) IBOutlet UIWebView *webView;

- (UIButton *)findButtonInView:(UIView *)view;


@end
