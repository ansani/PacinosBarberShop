//
//  StatusViewController.m
//  Facebook Demo
//
//  Created by Andy Yanok on 3/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FacebookStatusViewController.h"


@implementation FacebookStatusViewController

@synthesize statusMessage;


- (void) viewDidAppear:(BOOL)animated
{	
	self.title = @"Facebook";
	[self postToStatus:self];
}


- (void) viewDidDisappear:(BOOL)animated
{
}


- (IBAction)postToStatus:(id)sender
{
    
    
    //we will release this object when it is finished posting
//	FBFeedPost *postMessage = [[FBFeedPost alloc] initWithPostMessage:self.statusMessage];
//	[postMessage publishPostWithDelegate:self];
	
	//FBFeedPost *postImage = [[FBFeedPost alloc] initWithPhoto:[UIImage imageNamed:@"iTunesArtwork"] name:self.statusMessage];

	//FBFeedPost *postLink = [[FBFeedPost alloc] initWithLinkPath:@"http://pacinosbarbershop.com/app/" caption:self.statusMessage];
	FBFeedPost *postPost = [[FBFeedPost alloc] initWithPostMessage:self.statusMessage];
							//initWithLinkPath:@"http://www.pacinosbarbershop.com/app/" caption:self.statusMessage];

	
	//[postImage setCaption:self.statusMessage];
	[postPost publishPostWithDelegate:self];
	
	IFNNotificationDisplay *display = [[IFNNotificationDisplay alloc] init];
	display.type = NotificationDisplayTypeLoading;
	display.tag = NOTIFICATION_DISPLAY_TAG;
	[display setNotificationText:@"Posting Status..."];
	[display displayInView:self.view atCenter:CGPointMake(self.view.center.x, self.view.center.y-100.0) withInterval:0.0];
	[display release];
	
	
/*	
	
	//we will release this object when it is finished posting
	FBFeedPost *post = [[FBFeedPost alloc] initWithPostMessage:self.statusMessage];
	[post publishPostWithDelegate:self];
	
	IFNNotificationDisplay *display = [[IFNNotificationDisplay alloc] init];
	display.type = NotificationDisplayTypeLoading;
	display.tag = NOTIFICATION_DISPLAY_TAG;
	[display setNotificationText:@"Posting Status..."];
	[display displayInView:self.view atCenter:CGPointMake(self.view.center.x, self.view.center.y-100.0) withInterval:0.0];
	[display release];*/
}


#pragma mark -
#pragma mark FBFeedPostDelegate


- (void) failedToPublishPost:(FBFeedPost*) _post {

	UIView *dv = [self.view viewWithTag:NOTIFICATION_DISPLAY_TAG];
	[dv removeFromSuperview];
	
	IFNNotificationDisplay *display = [[IFNNotificationDisplay alloc] init];
	display.type = NotificationDisplayTypeText;
	[display setNotificationText:@"Failed To Post"];
	[display displayInView:self.view atCenter:CGPointMake(self.view.center.x, self.view.center.y-100.0) withInterval:1.5];
	[display release];
	
	//release the alloc'd post
	[_post release];
	NSLog(@"Failed to Post!");

	id alertView = [[UIAlertView alloc] initWithTitle:@"Failed to Post!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
	
}


- (void) finishedPublishingPost:(FBFeedPost*) _post {

	UIView *dv = [self.view viewWithTag:NOTIFICATION_DISPLAY_TAG];
	[dv removeFromSuperview];
	
	IFNNotificationDisplay *display = [[IFNNotificationDisplay alloc] init];
	display.type = NotificationDisplayTypeText;
	[display setNotificationText:@"Finished Posting"];
	[display displayInView:self.view atCenter:CGPointMake(self.view.center.x, self.view.center.y-100.0) withInterval:1.5];
	[display release];
	
	//release the alloc'd post
	[_post release];
	NSLog(@"Finished to Post!");
	
	id alertView = [[UIAlertView alloc] initWithTitle:@"Published status!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	[alertView release];
	
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark LoadView


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)dealloc {
    [super dealloc];
}


@end
