//
//  PacinosVideoViewController_iPad.m
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PacinosVideoViewController_iPad.h"
#import "YouTubeController.h"
#import "GData.h"
#import "GDataFeedPhotoAlbum.h"
#import "GDataFeedPhoto.h"
#import "FacebookStatusViewController.h"
#import "TwitterViewController.h"
#import "PacinosBarberShopAppDelegate_iPad.h"
#import "FBStatus_iPad.h"
#import "TWStatus_iPad.h"

#import "WebViewPlayer_iPad.h"
#import "WebViewMail_iPad.h"

#import "CustomTouchView.h"

#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>


@implementation PacinosVideoViewController_iPad

@synthesize currentIndexPath;

@synthesize bioImageArray;
@synthesize facebookStatusViewController;
@synthesize twitterController;
@synthesize webViewPlayer;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelFacebook:) name:@"CANCEL FACEBOOK" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(performReloadData:) name:@"ReloadData" object:nil];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
    
    
    //self.cancelBioAnimations = NO;
    self.bioImageArray = [NSArray arrayWithObjects:
                          @"photo(0).JPG",
                          @"photo(1).JPG",
                          @"photo(2).JPG",
                          @"photo(3).JPG",
                          @"photo(4).JPG", nil];
    
    //Keep um in a never ending cycle...
    
    [[searchBar.subviews objectAtIndex:0] removeFromSuperview];

    [self performSelectorOnMainThread:@selector(fadeImageLeft) withObject:nil waitUntilDone:NO];

}


- (void) viewWillAppear:(BOOL)animated
{    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}


- (void) performReloadData:(id)message
{
    [pacinosTableView reloadData];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}


- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [pacinosTableView reloadData];
}


#pragma mark -
#pragma mark UITableViewDelegate/Datasource Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    [youTubeController reloadFilteredArray];
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 220;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    int first = [[youTubeController filteredEntriesArray] count] / 4. + (([[youTubeController filteredEntriesArray] count] % 4) > 0 ? 1 : 0);
    int second = [[youTubeController filteredEntriesArray] count] / 3. + (([[youTubeController filteredEntriesArray] count] % 3) > 0 ? 1 : 0);
    
    NSLog(@"first = %i, second = %i", first, second);
    
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]))        
        return [[youTubeController filteredEntriesArray] count] / 4. + (([[youTubeController filteredEntriesArray] count] % 4) > 0 ? 1 : 0);
    return [[youTubeController filteredEntriesArray] count] / 3. + (([[youTubeController filteredEntriesArray] count] % 3) > 0? 1 : 0);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cachedCell"];
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, 320, 300) reuseIdentifier:@"cachedCell"] autorelease];
        
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        int offset = 256;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 14, 216, 130)];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.tag = 101;
        imageView.clipsToBounds = YES;
        imageView.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:imageView];
        
        [imageView release];
        
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 140, 140, 50)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:13];
        titleLabel.numberOfLines = 2;
        titleLabel.tag = 102;
        titleLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:titleLabel];
        
        [titleLabel release];
        
        
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 180, 180, 40)];
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.textColor = [UIColor darkGrayColor];
        descriptionLabel.font = [UIFont boldSystemFontOfSize:11];
        descriptionLabel.tag = 103;
        descriptionLabel.numberOfLines = 3;
        descriptionLabel.lineBreakMode = UILineBreakModeCharacterWrap;
        descriptionLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:descriptionLabel];
        
        [descriptionLabel release];
        
        
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 140, 80, 30)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [UIColor blackColor];
        timeLabel.font = [UIFont boldSystemFontOfSize:13];
        timeLabel.tag = 104;
        timeLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:timeLabel];
        
        [timeLabel release];
        
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20+offset, 14, 216, 130)];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.tag = 201;
        imageView.clipsToBounds = YES;
        imageView.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:imageView];
        
        [imageView release];
        
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20+offset, 140, 140, 50)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:13];
        titleLabel.numberOfLines = 2;
        titleLabel.tag = 202;
        titleLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:titleLabel];
        
        [titleLabel release];
        
        
        descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(20+offset, 180, 180, 40)];
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.textColor = [UIColor darkGrayColor];
        descriptionLabel.font = [UIFont boldSystemFontOfSize:11];
        descriptionLabel.tag = 203;
        descriptionLabel.numberOfLines = 3;
        descriptionLabel.lineBreakMode = UILineBreakModeCharacterWrap;
        descriptionLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:descriptionLabel];
        
        [descriptionLabel release];
        
        
        
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(160+offset, 140, 80, 30)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [UIColor blackColor];
        timeLabel.font = [UIFont boldSystemFontOfSize:13];
        timeLabel.tag = 204;
        timeLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:timeLabel];
        
        [timeLabel release];
        
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20+offset*2, 14, 216, 130)];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.tag = 301;
        imageView.clipsToBounds = YES;
        imageView.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:imageView];
        
        [imageView release];
        
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20+offset*2, 140, 140, 50)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:13];
        titleLabel.numberOfLines = 2;
        titleLabel.tag = 302;
        titleLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:titleLabel];
        
        [titleLabel release];
        
        
        descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(20+offset*2, 180, 180, 40)];
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.textColor = [UIColor darkGrayColor];
        descriptionLabel.font = [UIFont boldSystemFontOfSize:11];
        descriptionLabel.tag = 303;
        descriptionLabel.numberOfLines = 3;
        descriptionLabel.lineBreakMode = UILineBreakModeCharacterWrap;
        descriptionLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:descriptionLabel];
        
        [descriptionLabel release];
        
        
        
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(160+offset*2, 140, 80, 30)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [UIColor blackColor];
        timeLabel.font = [UIFont boldSystemFontOfSize:13];
        timeLabel.tag = 304;
        timeLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:timeLabel];
        
        [timeLabel release];
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20+offset*3, 14, 216, 130)];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.tag = 401;
        imageView.clipsToBounds = YES;
        imageView.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:imageView];
        
        [imageView release];
        
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20+offset*3, 140, 140, 50)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:13];
        titleLabel.numberOfLines = 2;
        titleLabel.tag = 402;
        titleLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:titleLabel];
        
        [titleLabel release];
        
        
        descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(20+offset*3, 180, 180, 40)];
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.textColor = [UIColor darkGrayColor];
        descriptionLabel.font = [UIFont boldSystemFontOfSize:11];
        descriptionLabel.tag = 403;
        descriptionLabel.numberOfLines = 3;
        descriptionLabel.lineBreakMode = UILineBreakModeCharacterWrap;
        descriptionLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:descriptionLabel];
        
        [descriptionLabel release];
        
        
        
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(160+offset*3, 140, 80, 30)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [UIColor blackColor];
        timeLabel.font = [UIFont boldSystemFontOfSize:13];
        timeLabel.tag = 404;
        timeLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:timeLabel];
        
        [timeLabel release];
        
        
        CustomTouchView *touchView = [[CustomTouchView alloc] initWithFrame:CGRectMake(0, 0, 1024, 220)];
        touchView.tag = 500;
        touchView.backgroundColor = [UIColor clearColor];
        touchView.videoTableView = tableView;
		touchView.caller = self;
		touchView.userInteractionEnabled = NO;
        
        [cell addSubview:touchView];
            
    }
    
    int currentImageIndex = 0;
    
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]))        
        currentImageIndex = indexPath.row*4;
    else
        currentImageIndex = indexPath.row*3;
    
    //-------------------------------------------------
    // IMAGE ENTRY
	
    
    id data = [[youTubeController imagesDictionary] valueForKey:[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"videoID"]];
    id image = nil;
    if (data != nil && data != [NSNull null])
        image = [UIImage imageWithData:(NSData *)data];
    
    if (image != nil)
        ((UIImageView *)[cell viewWithTag:101]).image = (UIImage *)image;
    else
        ((UIImageView *)[cell viewWithTag:101]).image = nil;
    
    //-------------------------------------------------
    
    ((UILabel *)[cell viewWithTag:102]).text = [[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] mediaTitle] stringValue];
    ((UILabel *)[cell viewWithTag:103]).text = [[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] mediaDescription] stringValue];
    ((UILabel *)[cell viewWithTag:104]).text = [self timeLabelWithDuration:[[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] duration] intValue]];
    
    cell.backgroundColor = [UIColor whiteColor];
	
    currentImageIndex++;
    
    if (currentImageIndex < [[youTubeController filteredEntriesArray] count]) 
    {
        //-------------------------------------------------
        // IMAGE ENTRY
        
        id data = [[youTubeController imagesDictionary] valueForKey:[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"videoID"]];
        id image = nil;
        if (data != nil && data != [NSNull null])
            image = [UIImage imageWithData:(NSData *)data];
        
        if (image != nil)
            ((UIImageView *)[cell viewWithTag:201]).image = (UIImage *)image;
        else
            ((UIImageView *)[cell viewWithTag:201]).image = nil;
        
        //-------------------------------------------------
        
        ((UILabel *)[cell viewWithTag:202]).text = [[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] mediaTitle] stringValue];
        ((UILabel *)[cell viewWithTag:203]).text = [[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] mediaDescription] stringValue];
        ((UILabel *)[cell viewWithTag:204]).text = [self timeLabelWithDuration:[[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] duration] intValue]];
        
    }
    else
    {
        ((UIImageView *)[cell viewWithTag:201]).image = nil;
        ((UILabel *)[cell viewWithTag:202]).text = @"";
        ((UILabel *)[cell viewWithTag:203]).text = @"";
        ((UILabel *)[cell viewWithTag:204]).text = @"";
        
    }

    currentImageIndex++;

    if (currentImageIndex < [[youTubeController filteredEntriesArray] count]) 
    {
        //-------------------------------------------------
        // IMAGE ENTRY
        
        id data = [[youTubeController imagesDictionary] valueForKey:[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"videoID"]];
        id image = nil;
        if (data != nil && data != [NSNull null])
            image = [UIImage imageWithData:(NSData *)data];
        
        if (image != nil)
            ((UIImageView *)[cell viewWithTag:301]).image = (UIImage *)image;
        else
            ((UIImageView *)[cell viewWithTag:301]).image = nil;
        
        //-------------------------------------------------
        
        ((UILabel *)[cell viewWithTag:302]).text = [[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] mediaTitle] stringValue];
        ((UILabel *)[cell viewWithTag:303]).text = [[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] mediaDescription] stringValue];
        ((UILabel *)[cell viewWithTag:304]).text = [self timeLabelWithDuration:[[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] duration] intValue]];
    }
    else
    {
        ((UIImageView *)[cell viewWithTag:301]).image = nil;
        ((UILabel *)[cell viewWithTag:302]).text = @"";
        ((UILabel *)[cell viewWithTag:303]).text = @"";
        ((UILabel *)[cell viewWithTag:304]).text = @"";

    }

    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]))
    {
        currentImageIndex++;

        if (currentImageIndex < [[youTubeController filteredEntriesArray] count]) 
        {
            //-------------------------------------------------
            // IMAGE ENTRY
            
            id data = [[youTubeController imagesDictionary] valueForKey:[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"videoID"]];
            id image = nil;
            if (data != nil && data != [NSNull null])
                image = [UIImage imageWithData:(NSData *)data];
            
            if (image != nil)
                ((UIImageView *)[cell viewWithTag:401]).image = (UIImage *)image;
            else
                ((UIImageView *)[cell viewWithTag:401]).image = nil;
            
            //-------------------------------------------------
            
            ((UILabel *)[cell viewWithTag:402]).text = [[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] mediaTitle] stringValue];
            ((UILabel *)[cell viewWithTag:403]).text = [[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] mediaDescription] stringValue];
            ((UILabel *)[cell viewWithTag:404]).text = [self timeLabelWithDuration:[[[[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup] duration] intValue]];
        }
        else
        {
            ((UIImageView *)[cell viewWithTag:401]).image = nil;
            ((UILabel *)[cell viewWithTag:402]).text = @"";
            ((UILabel *)[cell viewWithTag:403]).text = @"";
            ((UILabel *)[cell viewWithTag:404]).text = @"";   
        }
    }
    else
    {
        ((UIImageView *)[cell viewWithTag:401]).image = nil;
        ((UILabel *)[cell viewWithTag:402]).text = @"";
        ((UILabel *)[cell viewWithTag:403]).text = @"";
        ((UILabel *)[cell viewWithTag:404]).text = @"";        
    }

	return cell;
}


- (NSString *)timeLabelWithDuration:(int)duration
{
    int seconds = (duration % 60);
	int minutes = (duration / 60)/1;
	
	NSString *totalMinutesWithZeros;
	NSString *totalSecondsWithZeros;
	
	
	if ([[[NSNumber numberWithInt:minutes] stringValue] length] == 1)
	{
		totalMinutesWithZeros = [NSString stringWithFormat:@"0%i", minutes];
	}
	else {
		totalMinutesWithZeros = [[NSNumber numberWithInt:minutes] stringValue];
	}
	
	if ([[[NSNumber numberWithInt:seconds] stringValue] length] == 1)
	{
		totalSecondsWithZeros = [NSString stringWithFormat:@"0%i", seconds];
	}
	else {
		totalSecondsWithZeros = [[NSNumber numberWithInt:seconds] stringValue];
	}
	
    NSString *timeString = [NSString stringWithFormat:@"Time: %@:%@", totalMinutesWithZeros, totalSecondsWithZeros];
    return timeString;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSLog(@"Touched!");
	
    int currentImageIndex = 0;
     
    //int touchQuadrant = ((CustomTouchView *)[[tableView cellForRowAtIndexPath:indexPath] viewWithTag:500]).touchQuadrant;   
	
	int touchQuadrant = [(MyTableView*)tableView getTouchQuadrant];
    
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]))        
        currentImageIndex = indexPath.row*4 + touchQuadrant;
    else
        currentImageIndex = indexPath.row*3 + touchQuadrant;

        
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Start Network Loading" object:nil];
    
    if(!webViewPlayer)
        self.webViewPlayer = [WebViewPlayer_iPad new];
    
    
    UIView *loadingView = [(PacinosBarberShopAppDelegate_iPad *)[[UIApplication sharedApplication] delegate] loadingView];
    [loadingView removeFromSuperview];
    [webViewPlayer.view addSubview:loadingView];
    
    webViewPlayer.navigationItem.title = [[[[[youTubeController entriesFeed] entries] objectAtIndex:currentImageIndex] title] stringValue];
    
    NSLog(@"webViewPlayer = %@", [[webViewPlayer.view subviews] description]);   
    NSLog(@"webViewPlayer.webView = %@", [webViewPlayer.webView description]);
    
    self.currentIndexPath = indexPath;

    [self showYouTubeAtIndexPath:currentIndexPath];
    
    
//    NSLog(@"webViewPlayer = %@", [[webViewPlayer.view subviews] description]);   
//    NSLog(@"webViewPlayer.webView = %@", [webViewPlayer.webView description]);
    
    
    // This is a youtube video.  Put up an invisible UIWebView 
    /*
    NSString* embedHTML = @""
    "<html><head>"
    "<style type=\"text/css\">"
    "body {" 
    "background-color: transparent;"
    "color: white;"
    "}" 
    "</style>"
    "</head><body style=\"margin:0\">" 
    "<object width=\"%0.0f\" height=\"%0.0f\"><param name=\"movie\" value=\"%@&autoplay=1\">"
    "</param><embed src=\"%@&autoplay=1\" type=\"application/x-shockwave-flash\" width=\"%0.0f\" height=\"%0.0f\"></embed></object>"
    "</body></html>";  
    
    NSString *html = [NSString stringWithFormat:embedHTML,
                      webViewPlayer_iPad.webView.frame.size.width, 
                      webViewPlayer_iPad.webView.frame.size.height,
                      [flashContent URLString], 
                      [flashContent URLString],
                      webViewPlayer_iPad.webView.frame.size.width, 
                      webViewPlayer_iPad.webView.frame.size.height];
    
    html = [html stringByReplacingOccurrencesOfString:@"https" withString:@"http"];
    [webViewPlayer_iPad.webView loadHTMLString:html baseURL:nil];
    
    
    [self.navigationController pushViewController:webViewPlayer_iPad animated:YES];
    [webViewPlayer_iPad release];
    */
    
}


- (void) refreshWebView
{
    [self showYouTubeAtIndexPath:currentIndexPath];
}

- (void) processTouch:(int)quadrant {
	NSLog(@"Pressed: %i",quadrant);
}


- (void) showYouTubeAtIndexPath:(NSIndexPath *)indexPath
{
    int currentImageIndex = 0;
    
	int touchQuadrant = [(MyTableView*)pacinosTableView getTouchQuadrant];
    
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]))        
        currentImageIndex = indexPath.row*4 + touchQuadrant;
    else
        currentImageIndex = indexPath.row*3 + touchQuadrant;

    
    GDataYouTubeMediaGroup *mediaGroup = [[[[youTubeController filteredEntriesArray] objectAtIndex:currentImageIndex] valueForKey:@"entry"] mediaGroup];
    
    NSString *videoID = [mediaGroup videoID];    
    NSArray *mediaContents = [mediaGroup mediaContents];
    GDataMediaContent *flashContent =
    [GDataUtilities firstObjectFromArray:mediaContents
                               withValue:@"application/x-shockwave-flash"
                              forKeyPath:@"type"];
    
    NSLog(@"video ID = %@, flash content URL = %@",
          videoID, [flashContent URLString]);

    
    // This is a youtube video.  Put up an invisible UIWebView 
    
    
    NSString* embedHTML = @""
    "<html><head>"
    "<style type=\"text/css\">"
    "body {" 
    "background-color: black;"
    "color: black;"
    "}" 
    "</style>"
    "</head>"
        "<body style=\"margin:0\">" 
        "<meta name = \"viewport\" content = \"initial-scale = 1.0, maximum-scale=1.0, user-scalable = no, width=device-width\"/>"
    "</head>"
    "<body style=\"background:#000;margin-top:0px;margin-left:0px\">"    
        "<object width=\"%0.0f\" height=\"%0.0f\"><param name=\"movie\" value=\"%@&autoplay=1&controls=1\">"
        "</param><embed src=\"%@&autoplay=1&controls=1\" type=\"application/x-shockwave-flash\" width=\"%0.0f\" height=\"%0.0f\"></embed></object>"
    "</body></html>"
    
    "<script type=\"text/javascript\">"
   
    "function didRotateTo(ori) {"
        "window.location.reload()"
        ""
        "if (ori==\"portrait\") {"
            // ... do something
        "} else {"
            // ... do something else
        "}"
    "}"
/*    window.onload = function () {
        document.getElementById("div2").style.height = "100px";
        document.getElementById("div2").style.width = "100px";
    }
    
    function od() {
        var d1 = document.getElementById("div1");
        var d2 = document.getElementById("div2");
        alert("div1 dimensions: " + d1.offsetWidth + " x " + d1.offsetHeight);
        alert("div2 dimensions: " + d2.offsetWidth + " x " + d2.offsetHeight);
    }
    
    function sd() {
        var d1 = document.getElementById("div1");
        var d2 = document.getElementById("div2");
        alert("div1 dimensions: " + d1.style.width + " x " + d1.style.height);
        alert("div2 dimensions: " + d2.style.width + " x " + d2.style.height);
    }*/
    
    "</script>";  
    

    CGPoint frameSize;
    
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]))
        frameSize = CGPointMake(1024, 704);
    else
        frameSize = CGPointMake(768, 960);
      
    NSString *frameHTML = [NSString stringWithFormat:embedHTML,
                              frameSize.x, 
                              frameSize.y,
                              [flashContent URLString], 
                              [flashContent URLString],
                              frameSize.x, 
                              frameSize.y];
    
    
    frameHTML = [frameHTML stringByReplacingOccurrencesOfString:@"https" withString:@"http"];
    
    [webViewPlayer.webView loadHTMLString:frameHTML baseURL:nil];
    
    [self.navigationController pushViewController:webViewPlayer animated:YES];
    
}


#pragma mark -


- (IBAction)biographyAction:(id)sender
{
    if (biographyView.hidden == YES)
    {
        biographyView.hidden = NO;
        biographyView.alpha = 0.0;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.33];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(didStartBiography)];
        
        [biographyView setAlpha:1.0];
        
        [UIView commitAnimations];
    }    
}


- (void) didStartBiography
{
    currentBioImage = 0;
    
    [biographyTextView scrollRectToVisible:CGRectMake(0, 0, biographyTextView.frame.size.width, biographyTextView.frame.size.height) animated:NO];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:20.0];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelay:4.5];
    
    [biographyTextView scrollRectToVisible:CGRectMake(0, 300, biographyTextView.frame.size.width, biographyTextView.frame.size.height) animated:NO];
    
    [UIView commitAnimations];
    
    biographyTextView.userInteractionEnabled = YES;
    
}


- (void) fadeImageLeft
{
    
    [imageViewLeft setImage:[self nextBioImage]];
    [imageViewLeft setAlpha:0.0];
//    [imageViewLeft setTransform:CGAffineTransformIdentity];
    imageViewLeft.layer.transform = CATransform3DIdentity;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(finishedFadingImageLeft)];
    
//    CGAffineTransform translation = CGAffineTransformMakeTranslation(50, 35);
//    CGAffineTransform scale = CGAffineTransformMakeScale(1.5, 1.5);
//    [imageViewLeft setTransform:CGAffineTransformConcat(translation, scale)];
    CATransform3D translation = CATransform3DMakeTranslation(50, 35, 0);
    CATransform3D scale = CATransform3DMakeScale(1.5, 1.5, 1.0); 
    imageViewLeft.layer.transform = CATransform3DConcat(translation, scale);
    
    imageViewLeft.alpha = 1.0;
    
    [UIView commitAnimations];
}


- (void) finishedFadingImageLeft
{
    [imageViewRight setImage:[self nextBioImage]];
    [imageViewRight setAlpha:0.0];
//    [imageViewRight setTransform:CGAffineTransformIdentity];
    imageViewRight.layer.transform = CATransform3DIdentity;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(finishedFadingImageRight)];
    
//    CGAffineTransform translation = CGAffineTransformMakeTranslation(-50, 35);
//    CGAffineTransform scale = CGAffineTransformMakeScale(1.5, 1.5);
//    [imageViewRight setTransform:CGAffineTransformConcat(translation, scale)];
    CATransform3D translation = CATransform3DMakeTranslation(-50, 35, 0);
    CATransform3D scale = CATransform3DMakeScale(1.5, 1.5, 1.0);
    imageViewRight.layer.transform = CATransform3DConcat(translation, scale);

    
    imageViewRight.alpha = 1.0;
    
    [UIView commitAnimations];
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    
    [imageViewLeft setAlpha:0.0];
    
    [UIView commitAnimations];
    
}


- (void) fadeImageRight
{
    NSLog(@"FadeImageRight");
    
    [imageViewRight setImage:[self nextBioImage]];
    [imageViewRight setAlpha:0.0];
//    [imageViewRight setTransform:CGAffineTransformIdentity];
    imageViewRight.layer.transform = CATransform3DIdentity;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(finishedFadingImageRight)];
    
//    CGAffineTransform translation = CGAffineTransformMakeTranslation(-50, 35);
//    CGAffineTransform scale = CGAffineTransformMakeScale(1.5, 1.5);
//    [imageViewRight setTransform:CGAffineTransformConcat(translation, scale)];
    CATransform3D translation = CATransform3DMakeTranslation(-50, 35, 0);
    CATransform3D scale = CATransform3DMakeScale(1.5, 1.5, 1.0);
    imageViewLeft.layer.transform = CATransform3DConcat(translation, scale);

    imageViewRight.alpha = 1.0;
    
    [UIView commitAnimations];
}


- (void) finishedFadingImageRight
{
    [self fadeImageLeft];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    
    [imageViewRight setAlpha:0.0];
    
    [UIView commitAnimations];
}


- (void) fadeImageRightAfter5Seconds
{
    [self performSelector:@selector(fadeImageRight) withObject:nil afterDelay:5];
}


- (UIImage *) nextBioImage
{
    currentBioImage++;
    
    if (currentBioImage >= [bioImageArray count])
        currentBioImage = 0;
    
    return [UIImage imageNamed:[bioImageArray objectAtIndex:currentBioImage]]; //Assuming we hvae at least 1 image in the array   
}


- (IBAction)videoAction:(id)sender
{
    NSLog(@"Video Action");
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.33];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(hideBiographyViewDidFinish)];
    
    [biographyView setAlpha:0.0];
    
    [UIView commitAnimations];
    
    
}


- (void) hideBiographyViewDidFinish
{
    biographyView.hidden = YES;
}


- (void) cancelFacebook:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) postOnFB:(NSString*)message {
	NSLog(@"POST SU FB!!!");
	[self.facebookStatusViewController setStatusMessage:message];
	[self.navigationController pushViewController:facebookStatusViewController animated:YES];

}

- (void) postOnTW:(NSString*)message {
	NSLog(@"POST SU TW!!!");
	NSLog(@"Il messaggio è %@",message);
	
    self.twitterController = nil;
    
	if (!self.twitterController)
		self.twitterController = [TwitterViewController new];
	
	[twitterController setTweetMessage:message];
	[self.navigationController pushViewController:twitterController animated:NO];
	
}


- (IBAction)facebookAction:(id)sender
{
    NSLog(@"Facebook Action");
	
	if(!self.facebookStatusViewController)
		self.facebookStatusViewController = [FacebookStatusViewController new];
	
	
	FBStatus_iPad *msgFB = [[FBStatus_iPad alloc] initWithNibName:@"FBStatus_iPad" bundle:nil];
	[msgFB setCaller:self];
	[msgFB setModalPresentationStyle:UIModalPresentationFormSheet];
	[self presentModalViewController:msgFB animated:YES];
	[msgFB release];


}


- (IBAction)twitterAction:(id)sender
{
	NSLog(@"Twitter Action");
  
/*	
	self.twitterController = nil;
    
	if (!self.twitterController)
		self.twitterController = [TwitterViewController new];
	
	[twitterController setTweetMessage:@"Check out this app, http://pacinosbarbershop.com/app/"];
	[self.navigationController pushViewController:twitterController animated:YES];
*/
	
    
	TWStatus_iPad *msgTW = [[TWStatus_iPad alloc] initWithNibName:@"TWStatus_iPad" bundle:nil];
	[msgTW setCaller:self];
	//[msgTW setModalPresentationStyle:UIModalPresentationFormSheet];
	[self presentModalViewController:msgTW animated:YES];
	[msgTW release];
	
}


- (IBAction)mailAction:(id)sender
{
    WebViewMail_iPad *webViewPlayer = [WebViewMail_iPad new];
    
    webViewPlayer.navigationItem.title = @"Mail";
    
    NSLog(@"webViewPlayer = %@", [[webViewPlayer.view subviews] description]);   
    NSLog(@"webViewPlayer.webView = %@", [webViewPlayer.webView description]);
    
    [webViewPlayer.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.pacinosbarbershop.com/app/form.html"]]];
    
    [self.navigationController pushViewController:webViewPlayer animated:YES];
    [webViewPlayer release];    
}


#pragma mark -
#pragma mark UISearchBarDelegate Methods


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"textDidChange %@", youTubeController.searchFilterTerm);    
    youTubeController.searchFilterTerm = searchText;
    [pacinosTableView reloadData];
}


- (BOOL)searchBar:(UISearchBar *)theSearchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{    
    if ([text isEqualToString:@"\n"])
    {
        [theSearchBar resignFirstResponder];
        return NO;
    }
    return YES;
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)theSearchBar
{
    NSLog(@"CancelButtonClicked");    
    [theSearchBar resignFirstResponder];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    NSLog(@"SearchButtonClicked");
    [theSearchBar resignFirstResponder];    
}

@end
