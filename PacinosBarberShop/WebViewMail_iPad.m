//
//  WebViewMail_iPad.m
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 10/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "WebViewMail_iPad.h"

@implementation WebViewMail_iPad
@synthesize webView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelRequest:) name:@"Cancel Network Loading" object:nil];
   
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void) cancelRequest:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    cancelNetworkLoading = YES;
}


- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
 
    cancelNetworkLoading = NO;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES; //(interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark -
#pragma mark UIWebViewDelegate Methods


- (void)webViewDidFinishLoad:(UIWebView *)_webView {
    
    if (!cancelNetworkLoading)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Stop Network Loading" object:nil];
        
        UIButton *b = [self findButtonInView:_webView];
        [b sendActionsForControlEvents:UIControlEventTouchUpInside];        
    }
}


- (UIButton *)findButtonInView:(UIView *)view
{
	UIButton *button = nil;
	
	if ([view isMemberOfClass:[UIButton class]]) {
		return (UIButton *)view;
	}
	
	if (view.subviews && [view.subviews count] > 0) {
		for (UIView *subview in view.subviews) {
			button = [self findButtonInView:subview];
			if (button) return button;
		}
	}
	
	return button;
}


@end
