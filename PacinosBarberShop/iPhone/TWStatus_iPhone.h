//
//  TWStatus_iPhone.h
//  PacinosBarberShop
//
//  Created by Salvatore Ansani on 27/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TWStatus_iPhone : UIViewController{
	IBOutlet UITextView *myTextView;
}

-(void)setCaller:(id)caller;
-(IBAction)cancel:(id)sender;
-(IBAction)post:(id)sender;

@end
