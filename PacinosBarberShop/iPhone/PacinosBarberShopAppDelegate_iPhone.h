//
//  PacinosBarberShopAppDelegate_iPhone.h
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PacinosBarberShopAppDelegate.h"


@class PacinosVideoViewController_iPhone;

@interface PacinosBarberShopAppDelegate_iPhone : PacinosBarberShopAppDelegate
{
    IBOutlet UINavigationController *pacinosNavigationViewController;
    IBOutlet PacinosVideoViewController_iPhone *pacinosViewController;
    IBOutlet UIView *loadingView;
    
}
@property (readwrite, retain) IBOutlet UIView *loadingView;

- (IBAction)cancel:(id)sender;

@end
