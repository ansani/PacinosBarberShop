//
//  TexterauntTwitterViewController.h
//  GoTexan
//
//  Created by 9r0ximi7y on 4/21/11.
//  Copyright 2011 Appiction LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"

@interface TwitterViewController : UIViewController <SA_OAuthTwitterControllerDelegate, UIAlertViewDelegate> {

	IBOutlet UIActivityIndicatorView *activityIndicator;
	
	SA_OAuthTwitterEngine *_engine;
	NSMutableArray *tweets;
	
	id tweetMessage;
	
	int currentTweetStage;
}
@property (readwrite, retain) id tweetMessage;

- (IBAction) updateStream:(id)sender;
- (IBAction) tweet:(id)sender;

- (void) loadTwitter;

- (void) OAuthTwitterController: (SA_OAuthTwitterController *) controller authenticatedWithUsername: (NSString *) username;
- (void) OAuthTwitterControllerFailed: (SA_OAuthTwitterController *) controller;
- (void) OAuthTwitterControllerCanceled: (SA_OAuthTwitterController *) controller;


@end
