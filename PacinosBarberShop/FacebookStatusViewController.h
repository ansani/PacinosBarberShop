//
//  StatusViewController.h
//  Facebook Demo
//
//  Created by Andy Yanok on 3/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBFeedPost.h"
#import "IFNNotificationDisplay.h"

@interface FacebookStatusViewController : UIViewController <FBFeedPostDelegate, UIAlertViewDelegate> {
	NSString *statusMessage;
}
@property (readwrite, retain) NSString *statusMessage;


- (IBAction)postToStatus:(id)sender;

@end
