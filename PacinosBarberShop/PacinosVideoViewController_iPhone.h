//
//  PacinosVideoViewController_iPhone.h
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TwitterViewController;
@class FacebookStatusViewController;
@class YouTubeController;
@class WebViewPlayer;


@interface PacinosVideoViewController_iPhone : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
{
    IBOutlet UITableView *pacinosTableView;
    IBOutlet YouTubeController *youTubeController;
    
    IBOutlet UISearchBar *searchBar;
    
    
    FacebookStatusViewController *facebookStatusViewController;
    TwitterViewController *twitterController;
    IBOutlet WebViewPlayer *webViewPlayer;
    
    IBOutlet UIView *biographyView;
    IBOutlet UIImageView *imageViewLeft;
    IBOutlet UIImageView *imageViewRight;    
    IBOutlet UITextView *biographyTextView;
    NSArray *bioImageArray;
    
    int currentBioImage;
    
    NSIndexPath *currentIndexPath;
    
}
@property (readwrite, retain) NSIndexPath *currentIndexPath;

@property (readwrite, retain) NSArray *bioImageArray;
@property (nonatomic, retain) FacebookStatusViewController *facebookStatusViewController;
@property (nonatomic, retain) TwitterViewController *twitterController;
@property (readwrite, retain) WebViewPlayer *webViewPlayer;
- (void) showYouTubeAtIndexPath:(NSIndexPath *)indexPath;

- (IBAction)biographyAction:(id)sender;
- (IBAction)videoAction:(id)sender;
- (IBAction)facebookAction:(id)sender;
- (IBAction)twitterAction:(id)sender;
- (IBAction)mailAction:(id)sender;

- (void) fadeImageLeft;
- (void) fadeImageRight;
- (UIImage *) nextBioImage;
- (void) refreshWebView;

- (void) postOnFB:(NSString*)message;
- (void) postOnTW:(NSString*)message;


@end
