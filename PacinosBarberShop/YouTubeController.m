//
//  YouTubeController.m
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "YouTubeController.h"
#import "GDataServiceGoogleYouTube.h"
#import "GDataEntryPhotoAlbum.h"
#import "GDataEntryPhoto.h"
#import "GDataFeedPhoto.h"
#import "GDataEntryYouTubeUpload.h"
#import "GTMOAuth2ViewControllerTouch.h"

static NSString *const kKeychainItemName = @"PacinosBarberShop";


@implementation YouTubeController

@synthesize imagesDictionary;
@synthesize filteredEntriesArray;
@synthesize currentTargetVideoID;
@synthesize searchFilterTerm;


- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


- (void) awakeFromNib
{
    NSString *clientID = @"342656942911.apps.googleusercontent.com";
    NSString *clientSecret = @"qafOxX3QtCEBWWaLbsvGewfE";
    
    GTMOAuth2Authentication *auth;
    
    auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                                 clientID:clientID
                                                             clientSecret:clientSecret];
    self.searchFilterTerm = @"";
    
    self.imagesDictionary = [NSMutableDictionary new];
    self.filteredEntriesArray = [NSMutableArray new];
    
    [[self youTubeService] setAuthorizer:auth];
    [self fetchAllEntries];
}


- (void) reloadFilteredArray
{
    self.filteredEntriesArray = [NSMutableArray new];
    int filteredArrayCount = 0;
    
    //NSLog(@"SearchFilterTerm = %@", searchFilterTerm);
    
    for (id entry in [[self entriesFeed] entries])
    {
        if ([[self searchFilterTerm] isEqualToString:@""])
        {
            [filteredEntriesArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                             entry, @"entry",
                                             [[entry mediaGroup] videoID], @"videoID",
                                             nil]];
            if (![imagesDictionary valueForKey:[[entry mediaGroup] videoID]])
                [imagesDictionary setValue:[NSNull null] forKey:[[entry mediaGroup] videoID]];
            filteredArrayCount++;
        }
        else
        {
            NSString *description = [[[entry mediaGroup] mediaDescription] stringValue];
            NSString *keywords = [[[entry mediaGroup] mediaKeywords] stringValue];
            NSString *title = [[[entry mediaGroup] mediaTitle] stringValue];
            
            //NSLog(@"Description = %@", description);
            //NSLog(@"Keywords = %@", keywords);
            //NSLog(@"Title = %@", title);
            
            if (([description rangeOfString:searchFilterTerm options:NSCaseInsensitiveSearch].location != NSNotFound) ||
                ([keywords rangeOfString:searchFilterTerm options:NSCaseInsensitiveSearch].location != NSNotFound) ||
                ([title rangeOfString:searchFilterTerm options:NSCaseInsensitiveSearch].location != NSNotFound) )
            {
                [filteredEntriesArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                 entry, @"entry",
                                                 [[entry mediaGroup] videoID], @"videoID",
                                                 nil]];
                if (![imagesDictionary valueForKey:[[entry mediaGroup] videoID]])                
                    [imagesDictionary setValue:[NSNull null] forKey:[[entry mediaGroup] videoID]];                
                filteredArrayCount++;
            }
        }
        
    }
    //NSLog(@"Filtered Array Count = %i", filteredArrayCount);
}



#pragma mark -


// get a YouTube service object with the current username/password
//
// A "service" object handles networking tasks.  Service objects
// contain user authentication information as well as networking
// state information (such as cookies and the "last modified" date for
// fetched data.)

- (GDataServiceGoogleYouTube *)youTubeService {
	
	static GDataServiceGoogleYouTube* service = nil;
	
	if (!service) {
		service = [[GDataServiceGoogleYouTube alloc] init];
		
//		[service setShouldCacheDatedData:YES];
		[service setServiceShouldFollowNextLinks:YES];
		[service setIsServiceRetryEnabled:YES];
	}
	
	// update the username/password each time the service is requested
	NSString *username = @"pacinossalon@gmail.com";
	NSString *password = @"iloveapps";

	
	if ([username length] > 0 && [password length] > 0) {
		[service setUserCredentialsWithUsername:username
									   password:password];
	} else {
		// fetch unauthenticated
		[service setUserCredentialsWithUsername:nil
									   password:nil];
	}
	
	//Pacinos dev key = AI39si5cOpBrCVTMHkDwqqNPjzZPTWpOpbPTBERPvWGIr18-_5en3ajvQvwKDXAgnDnBD-MpkA9q28LHwxS4aJ0x11H6uK_12A
	[service setYouTubeDeveloperKey:@"AI39si5cOpBrCVTMHkDwqqNPjzZPTWpOpbPTBERPvWGIr18-_5en3ajvQvwKDXAgnDnBD-MpkA9q28LHwxS4aJ0x11H6uK_12A"];
	
	return service;
}


#pragma mark Fetch all entries


// begin retrieving the list of the user's entries
- (void)fetchAllEntries {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Start Network Loading" object:nil];
    
    [self setEntriesFeed:nil];
    [self setEntriesFetchError:nil];
    [self setEntriesFetchTicket:nil];
    
    GDataServiceGoogleYouTube *service = [self youTubeService];
    GDataServiceTicket *ticket;
    
    // feedID is uploads, favorites, etc
    //
    // note that activity feeds require a developer key
    NSString *feedID = @"uploads";
    
    NSURL *feedURL;
   
        feedURL = [GDataServiceGoogleYouTube youTubeURLForUserID:kGDataServiceDefaultUser
                                                      userFeedID:feedID];
    
    
    ticket = [service fetchFeedWithURL:feedURL
                              delegate:self
                     didFinishSelector:@selector(entryListFetchTicket:finishedWithFeed:error:)];
    
    [self setEntriesFetchTicket:ticket];
    
    //[[[[UIApplication sharedApplication] delegate] cancelImageButton] setHidden:NO];
    
    [self updateUI];
}


// feed fetch callback
- (void)entryListFetchTicket:(GDataServiceTicket *)ticket
            finishedWithFeed:(GDataFeedBase *)feed
                       error:(NSError *)error {
    
    [self setEntriesFeed:feed];
    [self setEntriesFetchError:error];
    [self setEntriesFetchTicket:nil];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Stop Network Loading" object:nil];
    
    [self updateUI];
}


- (void) updateUI
{
    GDataFeedBase *entriesFeed = [self entriesFeed];
    
    NSLog(@"Entries count = %i", [[entriesFeed entries] count]);
    
    if ([[entriesFeed entries] count] > 0)
    {
        currentImageFetchIndex = 0;
        GDataEntryBase *entry = [[mEntriesFeed entries] objectAtIndex:currentImageFetchIndex];
        
        //PULL ALL IMAGES INTO AN ARRAY
        
        if (!entry || ![entry respondsToSelector:@selector(mediaGroup)]) {
            
            // clear the image; no entry is selected, or it's not an entry type with a
            // thumbnail
            
            //SET NO IMAGE
            
        } else {
            // if the new thumbnail URL string is different from the previous one,
            // save the new URL, clear the existing image and fetch the new image
            GDataEntryYouTubeVideo *video = (GDataEntryYouTubeVideo *)entry;
            
            GDataMediaThumbnail *thumbnail = [[video mediaGroup] highQualityThumbnail];
            if (thumbnail != nil) {
                
                NSString *imageURLString = [thumbnail URLString];
                
                if (imageURLString) {
                    self.currentTargetVideoID = [[video mediaGroup] videoID];
                    [self fetchEntryImageURLString:imageURLString];
                }
            }
        }        
    }
}


- (void)fetchEntryImageURLString:(NSString *)urlString {
    GTMHTTPFetcher *fetcher = [GTMHTTPFetcher fetcherWithURLString:urlString];
    [fetcher setComment:@"thumbnail"];
    [fetcher beginFetchWithDelegate:self
                  didFinishSelector:@selector(imageFetcher:finishedWithData:error:)];
}


- (void)imageFetcher:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)data error:(NSError *)error {
    if (error == nil) {
        // got the data; display it in the image view
        
//        UIImage *image = [[UIImage alloc] initWithData:data];
        
        [imagesDictionary setValue:data forKey:currentTargetVideoID];

//        [image release];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadData" object:nil];
        
        currentImageFetchIndex++;
        
        if ([[mEntriesFeed entries] count] > currentImageFetchIndex)
        {
            GDataEntryBase *entry = [[mEntriesFeed entries] objectAtIndex:currentImageFetchIndex];
            
            //PULL ALL IMAGES INTO AN ARRAY
            
            if (!entry || ![entry respondsToSelector:@selector(mediaGroup)]) {
                
                // clear the image; no entry is selected, or it's not an entry type with a
                // thumbnail
                
                //SET NO IMAGE
                
            } else {
                // if the new thumbnail URL string is different from the previous one,
                // save the new URL, clear the existing image and fetch the new image
                GDataEntryYouTubeVideo *video = (GDataEntryYouTubeVideo *)entry;
                
                GDataMediaThumbnail *thumbnail = [[video mediaGroup] highQualityThumbnail];
                if (thumbnail != nil) {
                    
                    NSString *imageURLString = [thumbnail URLString];
                    
                    if (imageURLString) {
                        self.currentTargetVideoID = [[video mediaGroup] videoID];                        
                        [self fetchEntryImageURLString:imageURLString];
                    }
                }
            } 
            
        }
        
    } else {
        NSLog(@"imageFetcher:%@ failedWithError:%@", fetcher,  error);
    }
}


#pragma mark -
#pragma mark Setters and Getters


- (GDataFeedBase *)entriesFeed {
	return mEntriesFeed; 
}


- (void)setEntriesFeed:(GDataFeedBase *)feed {
	[mEntriesFeed autorelease];
	mEntriesFeed = [feed retain];
}


- (NSError *)entryFetchError {
	return mEntriesFetchError; 
}


- (void)setEntriesFetchError:(NSError *)error {
	[mEntriesFetchError release];
	mEntriesFetchError = [error retain];
}


- (GDataServiceTicket *)entriesFetchTicket {
	return mEntriesFetchTicket; 
}


- (void)setEntriesFetchTicket:(GDataServiceTicket *)ticket {
	[mEntriesFetchTicket release];
	mEntriesFetchTicket = [ticket retain];
}


- (NSString *)entryImageURLString {
	return mEntryImageURLString;
}


- (void)setEntryImageURLString:(NSString *)str {
	[mEntryImageURLString autorelease];
	mEntryImageURLString = [str copy];
}


- (GDataServiceTicket *)uploadTicket {
	return mUploadTicket;
}


- (void)setUploadTicket:(GDataServiceTicket *)ticket {
	[mUploadTicket release];
	mUploadTicket = [ticket retain];
}


#pragma mark -


- (void) dealloc
{
 	[mEntriesFeed release];
	[mEntriesFetchError release];   
    
    [super dealloc];
}


@end