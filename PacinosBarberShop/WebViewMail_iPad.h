//
//  WebViewMail_iPad.h
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 10/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewMail_iPad : UIViewController
{
    IBOutlet UIWebView *webView;
    
    BOOL cancelNetworkLoading;
}
@property (readwrite, retain) IBOutlet UIWebView *webView;

- (UIButton *)findButtonInView:(UIView *)view;


@end
