//
//  CustomTouchView.m
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomTouchView.h"

@implementation CustomTouchView

@synthesize touchQuadrant;
@synthesize videoTableView;
@synthesize caller;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSLog(@"TOUCH DETECTED!!!");
	
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    
    if (touchLocation.x < 256)
    {
        self.touchQuadrant = 0;
    }
    else
    {
        if (touchLocation.x < 256*2)
        {
            self.touchQuadrant = 1;
        }
        else
        {
            if (touchLocation.x < 256*3)
            {
                self.touchQuadrant = 2;
            }
            else
            {
                self.touchQuadrant = 3;
            }
        }
    }

	[caller processTouch:touchQuadrant]; 
    [videoTableView touchesBegan:touches withEvent:event];
}


@end
