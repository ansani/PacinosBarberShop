//
//  PacinosVideoViewController_iPhone.m
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PacinosVideoViewController_iPhone.h"
#import "YouTubeController.h"
#import "GData.h"
#import "GDataFeedPhotoAlbum.h"
#import "GDataFeedPhoto.h"
#import "FacebookStatusViewController.h"
#import "TwitterViewController.h"
#import "PacinosBarberShopAppDelegate_iPhone.h"
#import "FBStatus_iPhone.h"
#import "TWStatus_iPhone.h"

#import "WebViewPlayer.h"
#import "WebViewMail.h"


@implementation PacinosVideoViewController_iPhone

@synthesize currentIndexPath;
@synthesize bioImageArray;
@synthesize facebookStatusViewController;
@synthesize twitterController;
@synthesize webViewPlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelFacebook:) name:@"CANCEL FACEBOOK" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(performReloadData:) name:@"ReloadData" object:nil];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];


    //self.cancelBioAnimations = NO;
    self.bioImageArray = [NSArray arrayWithObjects:
                          @"photo(0).JPG",
                          @"photo(1).JPG",
                          @"photo(2).JPG",
                          @"photo(3).JPG",
                          @"photo(4).JPG", nil];

    //Keep um in a never ending cycle...
    
    [[searchBar.subviews objectAtIndex:0] removeFromSuperview];
    
    [self performSelectorOnMainThread:@selector(fadeImageLeft) withObject:nil waitUntilDone:NO];
    
}


- (void) viewWillAppear:(BOOL)animated
{    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}
     

- (void) performReloadData:(id)message
{
    [pacinosTableView reloadData];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark -
#pragma mark UITableViewDelegate/Datasource Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    [youTubeController reloadFilteredArray];
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[youTubeController filteredEntriesArray] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cachedCell"];
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, 320, 300) reuseIdentifier:@"cachedCell"] autorelease];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 120, 90)];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.tag = 101;
        imageView.clipsToBounds = YES;
        imageView.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:imageView];
        
        [imageView release];
        
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(130, 0, 160, 30)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:13];
        titleLabel.tag = 102;
        titleLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:titleLabel];
        
        [titleLabel release];
        
        
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(130, 26, 160, 40)];
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.textColor = [UIColor darkGrayColor];
        descriptionLabel.font = [UIFont boldSystemFontOfSize:11];
        descriptionLabel.tag = 103;
        descriptionLabel.numberOfLines = 3;
        descriptionLabel.lineBreakMode = UILineBreakModeCharacterWrap;
        descriptionLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:descriptionLabel];
        
        [descriptionLabel release];
        
        
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(130, 60, 160, 30)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [UIColor blackColor];
        timeLabel.font = [UIFont boldSystemFontOfSize:13];
        timeLabel.tag = 104;
        timeLabel.userInteractionEnabled = NO;
        
        [cell.contentView addSubview:timeLabel];
        
        [timeLabel release];
        
    }
    
    
    //-------------------------------------------------
    // IMAGE ENTRY
    
    id data = [[youTubeController imagesDictionary] valueForKey:[[[youTubeController filteredEntriesArray] objectAtIndex:indexPath.row] valueForKey:@"videoID"]];
    id image = nil;
    if (data != nil && data != [NSNull null])
    {
        image = [UIImage imageWithData:(NSData *)data];
    }
    
    //NSLog(@"imageData = %@", image);
    
    if (image != nil)
        ((UIImageView *)[cell viewWithTag:101]).image = (UIImage *)image;
    else
        ((UIImageView *)[cell viewWithTag:101]).image = nil;
    
    //-------------------------------------------------
    
    ((UILabel *)[cell viewWithTag:102]).text = [[[[[[youTubeController filteredEntriesArray] objectAtIndex:indexPath.row] valueForKey:@"entry"] mediaGroup] mediaTitle] stringValue];
    ((UILabel *)[cell viewWithTag:103]).text = [[[[[[youTubeController filteredEntriesArray] objectAtIndex:indexPath.row] valueForKey:@"entry"] mediaGroup] mediaDescription] stringValue];
    
    int duration = [[[[[[youTubeController filteredEntriesArray] objectAtIndex:indexPath.row] valueForKey:@"entry"] mediaGroup] duration] intValue];    
    
    
	
	int seconds = (duration % 60);
	int minutes = (duration / 60)/1;
	
	NSString *totalMinutesWithZeros;
	NSString *totalSecondsWithZeros;
	
	
	if ([[[NSNumber numberWithInt:minutes] stringValue] length] == 1)
	{
		totalMinutesWithZeros = [NSString stringWithFormat:@"0%i", minutes];
	}
	else {
		totalMinutesWithZeros = [[NSNumber numberWithInt:minutes] stringValue];
	}
	
	if ([[[NSNumber numberWithInt:seconds] stringValue] length] == 1)
	{
		totalSecondsWithZeros = [NSString stringWithFormat:@"0%i", seconds];
	}
	else {
		totalSecondsWithZeros = [[NSNumber numberWithInt:seconds] stringValue];
	}
	
    NSString *timeString = [NSString stringWithFormat:@"Time: %@:%@", totalMinutesWithZeros, totalSecondsWithZeros];
    
    ((UILabel *)[cell viewWithTag:104]).text = timeString;

    cell.backgroundColor = [UIColor whiteColor];
	
	return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Start Network Loading" object:nil];
    
    if (!webViewPlayer)
        self.webViewPlayer = [WebViewPlayer new];

    UIView *loadingView = [(PacinosBarberShopAppDelegate_iPhone *)[[UIApplication sharedApplication] delegate] loadingView];
    [loadingView removeFromSuperview];
    [webViewPlayer.view addSubview:loadingView];

    [webViewPlayer setDelegate:self];
    
    webViewPlayer.navigationItem.title = [[[[[youTubeController entriesFeed] entries] objectAtIndex:indexPath.row] title] stringValue];

    NSLog(@"webViewPlayer = %@", [[webViewPlayer.view subviews] description]);   
    NSLog(@"webViewPlayer.webView = %@", [webViewPlayer.webView description]);
    
    self.currentIndexPath = indexPath;
    
    // This is a youtube video.  Put up an invisible UIWebView 
    
    [self showYouTubeAtIndexPath:currentIndexPath];
     
    
    
    
    /*
    UIWebView *youTubeWebView = webViewPlayer.webView;
    
    NSString* embedHTML = @""
    "<html><head>"
    "<style type=\"text/css\">"
    "body {" 
    "background-color: transparent;"
    "color: white;"
    "}" 
    "</style>"
    "</head><body style=\"margin:0\">" 
    "<object width=\"%0.0f\" height=\"%0.0f\"><param name=\"movie\" value=\"%@&autoplay=1\">"
    "</param><embed src=\"%@&autoplay=1\" type=\"application/x-shockwave-flash\" width=\"%0.0f\" height=\"%0.0f\"></embed></object>"
    "</body></html>";  
    NSString *html = [NSString stringWithFormat:embedHTML,
                      youTubeWebView.frame.size.width, 
                      youTubeWebView.frame.size.height,
                      [flashContent URLString], 
                      [flashContent URLString],
                      youTubeWebView.frame.size.width, 
                      youTubeWebView.frame.size.height];
    [youTubeWebView loadHTMLString:html baseURL:nil];  
    
    [self.navigationController pushViewController:webViewPlayer animated:YES];
    [webViewPlayer release];
     */
    
    /*
    
    NSString *embedHTML = @"\
    <html><head>\
	<style type=\"text/css\">\
	body {\
	background-color: transparent;\
	color: white;\
	}\
	</style>\
	</head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
	width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";
	NSString *html = [NSString stringWithFormat:embedHTML, [flashContent URLString], webViewPlayer.webView.frame.size.width, webViewPlayer.webView.frame.size.height];
	[webViewPlayer.webView loadHTMLString:html baseURL:nil];

    [self.navigationController pushViewController:webViewPlayer animated:YES];
    [webViewPlayer release];

     */
    /*
    NSString *embedHTML = @"\
    <html><head>\
	<style type=\"text/css\">\
	body {\
	background-color: transparent;\
	color: white;\
	}\
	</style>\
	</head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
	width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";
    
    NSString *htmlString = @"<html><head>
    <meta name = \"viewport\" content = \"initial-scale = 1.0, user-scalable = no, width = 212\"/></head>
    <body style=\"background:#F00;margin-top:0px;margin-left:0px\">
    <div><object width=\"212\" height=\"172\">
    <param name=\"movie\" value=\"http://www.youtube.com/v/oHg5SJYRHA0&f=gdata_videos&c=ytapi-my-clientID&d=nGF83uyVrg8eD4rfEkk22mDOl3qUImVMV6ramM\"></param>
    <param name=\"wmode\" value=\"transparent\"></param>
    <embed src=\"http://www.youtube.com/v/oHg5SJYRHA0&f=gdata_videos&c=ytapi-my-clientID&d=nGF83uyVrg8eD4rfEkk22mDOl3qUImVMV6ramM\"
    type=\"application/x-shockwave-flash\" wmode=\"transparent\" width=\"212\" height=\"172\"></embed>
    </object></div></body></html>";
    
    [webViewPlayer.webView loadHTMLString:htmlString baseURL:[NSURL URLWithString:@"http://www.your-url.com"]];

    */
}


- (void) refreshWebView
{
    [self showYouTubeAtIndexPath:currentIndexPath];
}


- (void) showYouTubeAtIndexPath:(NSIndexPath *)indexPath
{
    GDataYouTubeMediaGroup *mediaGroup = [[[[youTubeController filteredEntriesArray] objectAtIndex:indexPath.row] valueForKey:@"entry"] mediaGroup];
    
    NSString *videoID = [mediaGroup videoID];    
    NSArray *mediaContents = [mediaGroup mediaContents];
    GDataMediaContent *flashContent =
    [GDataUtilities firstObjectFromArray:mediaContents
                               withValue:@"application/x-shockwave-flash"
                              forKeyPath:@"type"];
    
    NSLog(@"video ID = %@, flash content URL = %@",
          videoID, [flashContent URLString]);
    

    NSString* embedHTML = @""
    "<html><head>"
    "<style type=\"text/css\">"
    "body {" 
    "background-color: transparent;"
    "color: black;"
    "}" 
    "</style>"
    "</head><body style=\"background:#000;margin-top:0px;margin-left:0px\">" 
    "<object width=\"%0.0f\" height=\"%0.0f\"><param name=\"movie\" value=\"%@&autoplay=1&controls=1\">"
    "</param><embed src=\"%@&autoplay=1&controls=1\" type=\"application/x-shockwave-flash\" width=\"%0.0f\" height=\"%0.0f\"></embed></object>"
    "</body></html>";  
    
    NSString *html = [NSString stringWithFormat:embedHTML,
                      webViewPlayer.webView.frame.size.width, 
                      webViewPlayer.webView.frame.size.height,
                      [flashContent URLString], 
                      [flashContent URLString],
                      webViewPlayer.webView.frame.size.width, 
                      webViewPlayer.webView.frame.size.height];
    
    NSLog(@"\n\n\n%@\n\n\n", [flashContent URLString]);
    
    html = [html stringByReplacingOccurrencesOfString:@"https" withString:@"http"];
    [webViewPlayer.webView loadHTMLString:html baseURL:nil];
    
    
    NSLog(@"\n\n\n%@\n\n\n", html);
    
    [self.navigationController pushViewController:webViewPlayer animated:YES];
    
}


#pragma mark -


- (IBAction)biographyAction:(id)sender
{
    if (biographyView.hidden == YES)
    {
        biographyView.hidden = NO;
        biographyView.alpha = 0.0;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.33];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(didStartBiography)];
        
        [biographyView setAlpha:1.0];
        
        [UIView commitAnimations];
    }    
}


- (void) didStartBiography
{
    currentBioImage = 0;
    
    [biographyTextView scrollRectToVisible:CGRectMake(0, 0, biographyTextView.frame.size.width, biographyTextView.frame.size.height) animated:NO];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:20.0];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelay:4.5];
    
    [biographyTextView scrollRectToVisible:CGRectMake(0, 300, biographyTextView.frame.size.width, biographyTextView.frame.size.height) animated:NO];
    
    [UIView commitAnimations];
    
    biographyTextView.userInteractionEnabled = YES;
        
}


- (void) fadeImageLeft
{
    [imageViewLeft setImage:[self nextBioImage]];
    [imageViewLeft setAlpha:0.0];
    [imageViewLeft setTransform:CGAffineTransformIdentity];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(finishedFadingImageLeft)];
    
    CGAffineTransform translation = CGAffineTransformMakeTranslation(50, 35);
    CGAffineTransform scale = CGAffineTransformMakeScale(1.5, 1.5);
    [imageViewLeft setTransform:CGAffineTransformConcat(translation, scale)];
    imageViewLeft.alpha = 1.0;
    
    [UIView commitAnimations];
}


- (void) finishedFadingImageLeft
{
    [imageViewRight setImage:[self nextBioImage]];
    [imageViewRight setAlpha:0.0];
    [imageViewRight setTransform:CGAffineTransformIdentity];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(finishedFadingImageRight)];
    
    CGAffineTransform translation = CGAffineTransformMakeTranslation(-50, 35);
    CGAffineTransform scale = CGAffineTransformMakeScale(1.5, 1.5);
    [imageViewRight setTransform:CGAffineTransformConcat(translation, scale)];
    imageViewRight.alpha = 1.0;
    
    [UIView commitAnimations];


    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    
    [imageViewLeft setAlpha:0.0];
    
    [UIView commitAnimations];

}


- (void) fadeImageRight
{
    NSLog(@"FadeImageRight");
    
    [imageViewRight setImage:[self nextBioImage]];
    [imageViewRight setAlpha:0.0];
    [imageViewRight setTransform:CGAffineTransformIdentity];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(finishedFadingImageRight)];
    
    CGAffineTransform translation = CGAffineTransformMakeTranslation(-50, 35);
    CGAffineTransform scale = CGAffineTransformMakeScale(1.5, 1.5);
    [imageViewRight setTransform:CGAffineTransformConcat(translation, scale)];
    imageViewRight.alpha = 1.0;
    
    [UIView commitAnimations];
}


- (void) finishedFadingImageRight
{
    [self fadeImageLeft];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    
    [imageViewRight setAlpha:0.0];
    
    [UIView commitAnimations];
}


- (void) fadeImageRightAfter5Seconds
{
    [self performSelector:@selector(fadeImageRight) withObject:nil afterDelay:5];
}


- (UIImage *) nextBioImage
{
    currentBioImage++;
    
    if (currentBioImage >= [bioImageArray count])
        currentBioImage = 0;
    
    return [UIImage imageNamed:[bioImageArray objectAtIndex:currentBioImage]]; //Assuming we hvae at least 1 image in the array   
}


- (IBAction)videoAction:(id)sender
{
    NSLog(@"Video Action");
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.33];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(hideBiographyViewDidFinish)];
    
    [biographyView setAlpha:0.0];
    
    [UIView commitAnimations];
    

}


- (void) hideBiographyViewDidFinish
{
    biographyView.hidden = YES;
}




- (void) postOnFB:(NSString*)message {
	NSLog(@"POST SU FB!!!");
	[self.facebookStatusViewController setStatusMessage:message];
	[self.navigationController pushViewController:facebookStatusViewController animated:YES];
	
}

- (void) postOnTW:(NSString*)message {
	NSLog(@"POST SU TW!!!");
	NSLog(@"Il messaggio è %@",message);
	
    self.twitterController = nil;
    
	if (!self.twitterController)
		self.twitterController = [TwitterViewController new];
	
	[twitterController setTweetMessage:message];
	[self.navigationController pushViewController:twitterController animated:NO];
	
}


- (IBAction)facebookAction:(id)sender
{
    NSLog(@"Facebook Action");
	
	if(!self.facebookStatusViewController)
		self.facebookStatusViewController = [FacebookStatusViewController new];
	
	
	FBStatus_iPhone *msgFB = [[FBStatus_iPhone alloc] initWithNibName:@"FBStatus_iPhone" bundle:nil];
	[msgFB setCaller:self];
	[self presentModalViewController:msgFB animated:YES];
	[msgFB release];
	
	
}


- (void) cancelFacebook:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (IBAction)twitterAction:(id)sender
{
	/*
	NSLog(@"Twitter Action");
	
	
	self.twitterController = nil;
    
	if (!self.twitterController)
		self.twitterController = [TwitterViewController new];
	
	[twitterController setTweetMessage:@"Check out this app, http://pacinosbarbershop.com/app/"];
	[self.navigationController pushViewController:twitterController animated:YES];
	
*/	
    NSLog(@"Twitter Action");
    
	TWStatus_iPhone *msgTW = [[TWStatus_iPhone alloc] initWithNibName:@"TWStatus_iPhone" bundle:nil];
	[msgTW setCaller:self];
	[self presentModalViewController:msgTW animated:YES];
	[msgTW release];
	 
	
}



- (IBAction)mailAction:(id)sender
{
    WebViewMail *webViewPlayer = [WebViewMail new];
    
    webViewPlayer.navigationItem.title = @"Mail";
    
    NSLog(@"webViewPlayer = %@", [[webViewPlayer.view subviews] description]);   
    NSLog(@"webViewPlayer.webView = %@", [webViewPlayer.webView description]);
    
    [webViewPlayer.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.pacinosbarbershop.com/app/form.html"]]];
    
    [self.navigationController pushViewController:webViewPlayer animated:YES];
    [webViewPlayer release];    
}


#pragma mark -
#pragma mark UISearchBarDelegate Methods


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"textDidChange %@", youTubeController.searchFilterTerm);    
    youTubeController.searchFilterTerm = searchText;
    [pacinosTableView reloadData];
}


- (BOOL)searchBar:(UISearchBar *)theSearchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{    
    if ([text isEqualToString:@"\n"])
    {
        [theSearchBar resignFirstResponder];
        return NO;
    }
    return YES;
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)theSearchBar
{
    NSLog(@"CancelButtonClicked");    
    [theSearchBar resignFirstResponder];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    NSLog(@"SearchButtonClicked");
    [theSearchBar resignFirstResponder];    
}


@end