//
//  FBStatus_iPad.m
//  PacinosBarberShop
//
//  Created by Salvatore Ansani on 27/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FBStatus_iPad.h"

@implementation FBStatus_iPad

id myCaller;


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

-(void)setCaller:(id)caller {
	myCaller = caller;
}

-(IBAction)cancel:(id)sender {
	[self dismissModalViewControllerAnimated:YES];
}

-(IBAction)post:(id)sender {
	[self dismissModalViewControllerAnimated:YES];
	[myCaller postOnFB:[myTextView text]];
}

@end
