//
//  TWStatus_iPad.m
//  PacinosBarberShop
//
//  Created by Salvatore Ansani on 27/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "TWStatus_iPad.h"

@implementation TWStatus_iPad

id myCaller;


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

-(void)setCaller:(id)caller {
	myCaller = caller;
}

-(IBAction)cancel:(id)sender {
	[self dismissModalViewControllerAnimated:YES];
}

-(IBAction)post:(id)sender {
	[self dismissModalViewControllerAnimated:YES];
	[myCaller postOnTW:[myTextView text]];
}

@end

