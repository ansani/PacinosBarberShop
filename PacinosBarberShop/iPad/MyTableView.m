//
//  MyTableView.m
//  PacinosBarberShop
//
//  Created by Salvatore Ansani on 28/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MyTableView.h"

@implementation MyTableView

@synthesize touchQuadrant;

-(int)getTouchQuadrant {
	return self.touchQuadrant;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSLog(@"TOUCH DETECTED!!!");
	
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    
    if (touchLocation.x < 256)
    {
        self.touchQuadrant = 0;
    }
    else
    {
        if (touchLocation.x < 256*2)
        {
            self.touchQuadrant = 1;
        }
        else
        {
            if (touchLocation.x < 256*3)
            {
                self.touchQuadrant = 2;
            }
            else
            {
                self.touchQuadrant = 3;
            }
        }
    }
	
	//[caller processTouch:touchQuadrant]; 
    [super touchesBegan:touches withEvent:event];
}


@end
