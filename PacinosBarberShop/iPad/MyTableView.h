//
//  MyTableView.h
//  PacinosBarberShop
//
//  Created by Salvatore Ansani on 28/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableView : UITableView {
	int touchQuadrant;
}

-(int)getTouchQuadrant;

@property (readwrite, assign) int touchQuadrant;

@end
