//
//  PacinosBarberShopAppDelegate_iPad.m
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PacinosBarberShopAppDelegate_iPad.h"

@implementation PacinosBarberShopAppDelegate_iPad

@synthesize loadingView;


- (void) awakeFromNib
{
    [self.window addSubview:pacinosNavigationViewController.view];   
    
    loadingView.alpha = 0.0;
    loadingView.hidden = YES;
    [self.window addSubview:loadingView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLoading) name:@"Start Network Loading" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideLoading) name:@"Stop Network Loading" object:nil];
    
}


- (IBAction)cancel:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Cancel Network Loading" object:nil];
}


- (void) showLoading
{
    loadingView.hidden = NO;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.33];
    
    loadingView.alpha = 1.0;
    
    [UIView commitAnimations];
}


- (void) hideLoading
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.33];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(finishedHiding)];
    
    loadingView.alpha = 0.0;
    
    [UIView commitAnimations];   
}


- (void) finishedHIding
{
    loadingView.hidden = YES;
}


@end
