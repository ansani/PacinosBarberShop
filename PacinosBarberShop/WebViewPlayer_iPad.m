//
//  WebViewPlayer_iPad.m
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "WebViewPlayer_iPad.h"
#import "PacinosVideoViewController_iPad.h"

@implementation WebViewPlayer_iPad

@synthesize webView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void) cancelRequest:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    cancelNetworkLoading = YES;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
 
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelRequest:) name:@"Cancel Network Loading" object:nil];

}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}


- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
//    [delegate refreshWebView]; // turn on if black is a problem again...
    [self updateWebViewOrientation];
}


- (void) updateWebViewOrientation
{
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"didRotateTo('%@')", [self currentOrientationAsString]]];
}


- (NSString*) currentOrientationAsString {
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]))
    {    
        return @"landscape";
    }
    return @"portrait"; // assuming portrait is default
}


- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    
    [self updateWebViewOrientation];
    
    cancelNetworkLoading = NO;
}


- (void) viewWillDisappear:(BOOL)animated
{
	NSLog(@"Muoio!");
	[self.webView loadHTMLString:nil baseURL:nil];

    cancelNetworkLoading = YES;
}


#pragma mark -
#pragma mark UIWebViewDelegate Methods


- (void)webViewDidFinishLoad:(UIWebView *)_webView {

    if (!cancelNetworkLoading)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Stop Network Loading" object:nil];
        
        UIButton *b = [self findButtonInView:_webView];
        [b sendActionsForControlEvents:UIControlEventTouchUpInside];
    }        

}


- (UIButton *)findButtonInView:(UIView *)view
{
	UIButton *button = nil;
	
	if ([view isMemberOfClass:[UIButton class]]) {
		return (UIButton *)view;
	}
	
	if (view.subviews && [view.subviews count] > 0) {
		for (UIView *subview in view.subviews) {
			button = [self findButtonInView:subview];
			if (button) return button;
		}
	}
	
	return button;
}


@end
