//
//  YouTubeController.h
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GData.h"
#import "GDataFeedPhotoAlbum.h"
#import "GDataFeedPhoto.h"


@interface YouTubeController : NSObject
{
    //---------------------------
	//------- GData Stuff -------
	GDataFeedYouTubeVideo *mEntriesFeed; // user feed of album entries
	GDataServiceTicket *mEntriesFetchTicket;
	NSError *mEntriesFetchError;
	NSString *mEntryImageURLString;
	
	GDataServiceTicket *mUploadTicket;

    NSMutableDictionary *imagesDictionary;    
    NSString *searchFilterTerm;
    NSMutableArray *filteredEntriesArray;

    NSString *currentTargetVideoID;
    
    int currentImageFetchIndex;
    //---------------------------

}

@property (readwrite, retain) NSString *searchFilterTerm;
@property (readwrite, retain) NSMutableDictionary *imagesDictionary;
@property (readwrite, retain) NSMutableArray *filteredEntriesArray;
@property (readwrite, retain) NSString *currentTargetVideoID;

- (void) reloadFilteredArray;


- (GDataServiceTicket *)uploadTicket;
- (void)setUploadTicket:(GDataServiceTicket *)ticket;
- (GDataServiceGoogleYouTube *)youTubeService;

- (void)fetchAllEntries;

- (GDataFeedBase *)entriesFeed;
- (void)setEntriesFeed:(GDataFeedBase *)feed;

//- (NSError *)entriesFetchError;
- (void)setEntriesFetchError:(NSError *)error;

- (GDataServiceTicket *)entriesFetchTicket;
- (void)setEntriesFetchTicket:(GDataServiceTicket *)ticket;

- (void) updateUI;

- (void)fetchEntryImageURLString:(NSString *)urlString;
- (void)imageFetcher:(GTMHTTPFetcher *)fetcher finishedWithData:(NSData *)data error:(NSError *)error;

@end
