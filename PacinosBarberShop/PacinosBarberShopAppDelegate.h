//
//  PacinosBarberShopAppDelegate.h
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PacinosBarberShopAppDelegate : NSObject <UIApplicationDelegate>

@property (nonatomic, retain) IBOutlet UIWindow *window;

@end
