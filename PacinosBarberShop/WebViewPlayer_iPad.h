//
//  WebViewPlayer_iPad.h
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PacinosVideoViewController_iPad;

@interface WebViewPlayer_iPad : UIViewController <UIWebViewDelegate>
{
    IBOutlet UIWebView *webView;    
    BOOL cancelNetworkLoading;
    PacinosVideoViewController_iPad *delegate;
}
@property (readwrite, retain) IBOutlet UIWebView *webView;


- (UIButton *)findButtonInView:(UIView *)view;
- (void) updateWebViewOrientation;

@end
