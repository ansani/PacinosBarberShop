//
//  TexterauntTwitterViewController.m
//  GoTexan
//
//  Created by 9r0ximi7y on 4/21/11.
//  Copyright 2011 Appiction LLC. All rights reserved.
//

#import "TwitterViewController.h"


@implementation TwitterViewController

@synthesize tweetMessage;


- (void) viewDidAppear:(BOOL)animated
{
	currentTweetStage = 0;
	[activityIndicator startAnimating];
	[self loadTwitter];
}


- (void) viewDidDisappear:(BOOL)animated
{
	[activityIndicator stopAnimating];
}


- (void) loadTwitter
{
	if (_engine)
		return;
	
	_engine = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];
	_engine.consumerKey = @"7Yigaam2BSNC4fhdyjaYA";
	_engine.consumerSecret = @"eme85wWEpd8LGJuanSIiHtHcjRIqXGOlrrIo4jpW2Y";
	
	UIViewController *controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:_engine delegate:self];
	if (controller) {
		//[controller setModalPresentationStyle:UIModalPresentationFormSheet];

		[self presentModalViewController:controller animated:YES];
		//		[self.presentedViewController presentModalViewController:controller animated:YES];
		//[self dismissModalViewControllerAnimated:YES];
		
	}
	else {
		tweets = [NSMutableArray new];
		[self updateStream:nil];
	}
}


- (IBAction) updateStream:(id)sender
{	
	[_engine getFollowedTimelineSinceID:1 startingAtPage:1 count:2];
}


- (IBAction) tweet:(id)sender
{
	[_engine sendUpdate:tweetMessage];
    [self updateStream:nil];
}


#pragma mark SA_OAuthTwitterEngineDelegate


- (void) storeCachedTwitterOAuthData:(NSString *)data forUsername:(NSString *)username
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	[defaults setObject:data forKey:@"authData"];
	[defaults synchronize];	
}


- (NSString *) cachedTwitterOAuthDataForUsername:(NSString *)username
{
	return [[NSUserDefaults standardUserDefaults] objectForKey:@"authData"];
}


#pragma mark SA_OAuthTwitterController Delegate


- (void) OAuthTwitterController: (SA_OAuthTwitterController *) controller authenticatedWithUsername: (NSString *) username
{	
	NSLog(@"Authenticated with user %@", username);
	
	tweets = [[NSMutableArray alloc] init];
	[self updateStream:nil];
}


- (void) OAuthTwitterControllerFailed: (SA_OAuthTwitterController *) controller
{	
	NSLog(@"Authentication Failure");
}


- (void) OAuthTwitterControllerCanceled: (SA_OAuthTwitterController *) controller
{
	NSLog(@"Authentication Canceled");
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark MGTwitterEngineDelegate Methods


- (void)requestSucceeded:(NSString *)connectionIdentifier {
	
	NSLog(@"Request Suceeded: %@", connectionIdentifier);
	if (currentTweetStage == 1)
	{
		currentTweetStage = 2;
		
		id alert = [[UIAlertView alloc] initWithTitle:@"Tweet Successful!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
	if (currentTweetStage == 0) {
		currentTweetStage = 1;
		[self tweet:self];
	}

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	[self.navigationController popViewControllerAnimated:YES];
}


- (void)statusesReceived:(NSArray *)statuses forRequest:(NSString *)connectionIdentifier {
	
	tweets = [[NSMutableArray alloc] init];
	
	for(NSDictionary *d in statuses) {
		
		//NSLog(@"See dictionary: %@", d);
		
		//Tweet *tweet = [[Tweet alloc] initWithTweetDictionary:d];
		//[tweets addObject:tweet];
		//[tweet release];
	}
	
	//[self.tableView reloadData];
}


- (void)receivedObject:(NSDictionary *)dictionary forRequest:(NSString *)connectionIdentifier {
	
	NSLog(@"Recieved Object: %@", dictionary);
}


- (void)directMessagesReceived:(NSArray *)messages forRequest:(NSString *)connectionIdentifier {
	
	NSLog(@"Direct Messages Received: %@", messages);
}


- (void)userInfoReceived:(NSArray *)userInfo forRequest:(NSString *)connectionIdentifier {
	
	NSLog(@"User Info Received: %@", userInfo);
}


- (void)miscInfoReceived:(NSArray *)miscInfo forRequest:(NSString *)connectionIdentifier {
	
	NSLog(@"Misc Info Received: %@", miscInfo);
}


@end
