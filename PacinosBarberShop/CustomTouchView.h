//
//  CustomTouchView.h
//  PacinosBarberShop
//
//  Created by 9r0ximi7y on 9/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTouchView : UIView
{
    int touchQuadrant;
    id videoTableView;
	id caller;
}

@property (readwrite, assign) int touchQuadrant;
@property (readwrite, retain) id videoTableView;
@property (readwrite, retain) id caller;

@end
